# OpenML dataset: Run_or_walk_information

https://www.openml.org/d/40922

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Viktor Malyi  
**Source**: [Kaggle](https://www.kaggle.com/vmalyi/run-or-walk)  
**Please cite**:   

**Run or walk**  
This dataset is gather to detect whether a person is running or walking based on deep neural networks and sensor data collected from iOS devices.

The dataset represents 88588 sensor data samples collected from the accelerometer and gyroscope from iPhone 5c in 10 seconds intervals and ~5.4/second frequency. 

### Attribute information  
This data is represented by following columns (each column contains sensor data for one of the sensor's axes):

acceleration_x
acceleration_y
acceleration_z
gyro_x
gyro_y
gyro_z

There is an activity type represented by "activity" column which acts as label and reflects following activities:

"0": walking
"1": running

The original data also contains a "wrist" column which represents the wrist where the device was placed, and "date", "time" and "username" columns which provide information about the exact date, time and user which collected these measurements.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40922) of an [OpenML dataset](https://www.openml.org/d/40922). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40922/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40922/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40922/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

